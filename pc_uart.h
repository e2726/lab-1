#ifndef PC_UART_H_INCLUDED
#define PC_UART_H_INCLUDED

#define UART_BAUD       115200
#define UART_BYTE_SIZE  8
#define UART_STOP       TWOSTOPBITS
#define UART_PARITY     EVENPARITY
#define RX_BUFFER_SIZE 256

unsigned char SerialBuffer[RX_BUFFER_SIZE];     //Buffer for storing Rxed Data
volatile int UARTexitFlag;

//Interrupt Vector
void intHandler(int dummy);

//Function Prototypes
int getCommInput(void);
void connectToComm(int commPort, HANDLE* hComm);
void setSerialParams(HANDLE*);
void writeComm(HANDLE* hComm, char* buffer);
void readComm(HANDLE* phComm, char*);


#endif // PC_UART_H_INCLUDED



