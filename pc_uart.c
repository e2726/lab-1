#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include "pc_uart.h"

void intHandler(int dummy)
{
    UARTexitFlag = 1;
}

/* --------------------------------------
* Description: Asks the user to enter a comm port number to
* connect to the MSP432 via UART.
* inputs: none
* outputs: current port number
---------------------------------------*/
int getCommInput(void)                                  // Ask user for comm port number
{

    int commPort = 0;
    // Ask user for port number
    printf("Enter the comm port as a number: ");
    scanf("%d", &commPort);
    return commPort;
}

/* --------------------------------------
* Description: Opens a serial UART console using the given
* port number.  Does error checking to make sure the port
* was opened correctly.
* inputs: comm port number, serial comm stream
* outputs: none
---------------------------------------*/
void connectToComm(int commPort, HANDLE* hComm)
{
    char commStr[13];
    sprintf(commStr, "COM%d",commPort);

    *hComm = CreateFile(commStr,                        // port name
                        GENERIC_READ | GENERIC_WRITE,   // Read/Write
                        0,                              // No Sharing
                        NULL,                           // No Security
                        OPEN_EXISTING,                  // Open existing port only
                        FILE_ATTRIBUTE_NORMAL,          // Non Overlapped I/O
                        NULL);                          // Null for Comm Devices

    if (*hComm == INVALID_HANDLE_VALUE)
    {
        printf("Error in opening %d serial port...Exiting\n\n", commPort);
        exit(-1);
    }
    else
    {
        printf("%d serial port opened successfully\n", commPort);
    }

    // Clear any existing data in the comm port
    BOOL success = PurgeComm(*hComm, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if (!success)
    {
        printf("Unable to purge comm port.\n");
        exit(-1);
    }

}

/* --------------------------------------
* Description: Gets the current comm state, if there is any,
* and sets the current comm state, specifically the baud rate
* of 115200.
* inputs: serial comm stream
* outputs: none
---------------------------------------*/
void setSerialParams(HANDLE* hComm)
{
    DCB dcbMaster = {0};
    BOOL success;

    success = GetCommState(*hComm, &dcbMaster);

    if(!success)
    {
        printf("Unable to retrieve previous serial parameters.\n");
    }

    dcbMaster.BaudRate = UART_BAUD;        // BaudRate = 115200
    dcbMaster.ByteSize = UART_BYTE_SIZE;   // ByteSize = 8
    dcbMaster.StopBits = UART_STOP;        // StopBits = 1
    dcbMaster.Parity   = UART_PARITY;      // NONE

    SetCommState(*hComm, &dcbMaster);

    Sleep(60);
}

/* --------------------------------------
* Description: Writes a message to the UART serial console
* stream.
* inputs: serial comm stream, buffer containing the
* data to be written
* outputs: none
---------------------------------------*/
void writeComm(HANDLE* hComm, char* buffer)
{
    DWORD nNumberOfBytesToWrite = strlen(buffer);   // Number of bytes to write to the console
    DWORD lpNumberOfBytesWritten = 0;               // receives back the number of bytes written
    BOOL success;

    success = WriteFile(*hComm,                     // Handle to the Serial port
                        buffer,                                 // Data to be written to the port
                        nNumberOfBytesToWrite,                  // length of data to write
                        &lpNumberOfBytesWritten,                // stores and returns number of bytes written
                        NULL);                                  // not needed

    if (!success)
    {
        printf("Could not write to serial console.\n");
    }
//    if (success)
//    {
//        printf("MSP432: %s\n", buffer);
//    }
    if (lpNumberOfBytesWritten != nNumberOfBytesToWrite)
    {
        printf("Not all bytes were written to the port.");
    }
}


/* --------------------------------------
* Description: Reads messages sent by the MSP432
* inputs: serial comm stream, string to store the read data in
* outputs: none
---------------------------------------*/
void readComm(HANDLE* hComm, char* readString)
{
    unsigned char TempChar;                 //Temporary character used for reading
    BOOL success;
    DWORD NoBytesRead;
    int i;

    //Clear read buffer
    for(i=0; i<RX_BUFFER_SIZE; i++)
        readString[i] = 0x00;

    i=0;
    do
    {
        success = ReadFile( *hComm,         // Handle of the Serial port
                            &TempChar,                     // Temporary character
                            1,                             // Size of TempChar
                            &NoBytesRead,                  // Number of bytes read
                            NULL);

        if(success && NoBytesRead)
        {
            readString[i] = TempChar;       // Store Tempchar into buffer
            i++;
        }
        else if(!success)
        {
            printf("Failed to read from comm\n");
            break;
        }
    }
    while (NoBytesRead > 0);

    if(success && readString[0] != 0x00)
        printf("%s\n", readString);
}
