/* ---------------------------------------------------------
* Authors: Kendra Francis and Leigha VanderKlok
* Date: 1-20-22
* Purpose: to interface with the MSP432 using UART.  The red
* LED on the MSP432 initially blinks at 60bpm and the user
* can increase or decrease the speed by pressing 'u' or 'd', respectively.
* The system can be reset to 60 bpm by pressing 'r'.
------------------------------------------------------------*/

#include <stdlib.h>
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include "pc_uart.h"


int main()
{
    HANDLE hComm;                                       // hComm stream

    int commPort = getCommInput();                      // Ask user for comm port number
    connectToComm(commPort, &hComm);
    setSerialParams(&hComm);                            // Set the serial port parameters

    char tempoInput = '\0';                             // selected input tempo
    char str[4];
    int currentTempo = 60;
    char readString[RX_BUFFER_SIZE];                    // Read Input String

    printf("Enter a u to increase by 2bpm, d to decrease by 2bpm and r to reset: ");

    while(!UARTexitFlag)
    {
        scanf("%c", &tempoInput);
        if (tempoInput == 'u')                          // increase the current tempo by 2 bpm
        {
            currentTempo += 2;
            sprintf(str, "%d\n", currentTempo);
            writeComm(&hComm, str);                     // send to MSP432 as a string
            tempoInput = '\0';
        }
        if (tempoInput == 'd')                          // decrease current tempo by 2 bpm
        {
            currentTempo -= 2;
            sprintf(str, "%d\n", currentTempo);
            writeComm(&hComm, str);
            tempoInput = '\0';
        }
        if (tempoInput == 'r')
        {
            currentTempo = 60;
            sprintf(str, "%d\n", currentTempo);
            writeComm(&hComm, str);
            tempoInput = '\0';
        }
        readComm(&hComm, readString);                   // read for input back from the MSP
    }

    CloseHandle(hComm);                                 // Close the Serial Port

    return 0;
}

